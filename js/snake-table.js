var tableCount = 0;

function clearTables() {
  document.getElementById('container').innerHTML = '';
  tableCount = 0;
}

function generateTable() {
  // User input values and number of cells calculation
  var rows = +document.getElementById('rows').value;
  var cols = +document.getElementById('columns').value;
  var cells = rows * cols;

  // create a new table element and add it to the container
  var tableElm = document.createElement('table');
  tableElm.setAttribute('id', 'table_' + tableCount);
  document.getElementById('container').appendChild(tableElm);

  // initialize the row counter and the insert direction
  // 1 = top -> down, -1 = down -> top
  var rowCount = 0;
  var direction = 1;

  // iterate based on the number of cells
  for (var i = 0; i < cells; i++) {
    var rowId = 'row_' + tableCount + '_' + rowCount;

    var rowElm = document.getElementById(rowId);
    if (!rowElm) {
      rowElm = document.createElement('tr');
      rowElm.setAttribute('id', rowId);
      tableElm.appendChild(rowElm);
    }

    var cellElm = document.createElement('td');
    cellElm.innerHTML = 'Cell: ' + (i + 1);

    rowElm.appendChild(cellElm);

    rowCount += 1 * direction;

    if (rowCount >= rows || rowCount < 0) {
      rowCount = direction > 0 ? rows - 1 : 0;
      direction *= -1;
    }
  }
  tableCount++;
}
